package com.github.patrykcd.libgdxSetupPlugin.gdx

import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Paths
import java.util.zip.ZipInputStream

class LibGDXProject(zipStream: ZipInputStream, gdxGradleBytes: ByteArray, outPath: String = "")
{
	private val path = outPath
	
	private var dataFiles = ArrayList<DataFile>()
	private var gradleFile: GradleFile
	
	val names: Array<String>
		get() = dataFiles.map { it.name }.toTypedArray()
	
	private val _subProjects = Constants.SubProject.values().toCollection(ArrayList())
	
	init
	{
		// INFO: ZipInputStream doesn't have reset method for iterator :(
		// split content from zip file into folders and files
		var entry = zipStream.nextEntry
		while (entry != null)
		{
			val entryPath = "$path${File.separator}${entry.name}"
			if (!entry.isDirectory) dataFiles.add(DataFile(entryPath, zipStream.readBytes()))
			entry = zipStream.nextEntry
		}
		
		// INFO: there closed because it is only one-time usable because doesn't contain reset method for iterator
		// close stream
		zipStream.apply {
			closeEntry()
			close()
		}
		
		gradleFile = GradleFile(gdxGradleBytes, path)
	}
	
	fun removeSubProject(subProject: Constants.SubProject)
	{
		_subProjects.remove(subProject)
		
		dataFiles
				.filter { it.path.contains(subProject.module) }
				.forEach { dataFiles.remove(it) }
		
		// TODO only for one
		gradleFile.removeSubProjects(listOf(subProject))
	}
	
	fun removeExtensions(extensions: Constants.Extensions)
	{
		gradleFile.removeExtensions(listOf(extensions))
	}
	
	fun removeExtraFileForHtml()
	{
	}
	
	fun create()
	{
		dataFiles.add(gradleFile.dataFile)
		//println("dsasd ==== \n "+ gradleFile.dataFile.path + "    vw  " + dataFiles.first().path)
		// create folders first...
		File(path).mkdirs()
		dataFiles.forEach {
			val folderPath = Paths.get(it.path).parent.toString()
			File(folderPath).mkdirs()
		}
		
		// ...and then files
		for ((path, bytes) in dataFiles)
		{
			File(path).apply {
				createNewFile()
				FileOutputStream(this).apply {
					write(bytes)
					close()
				}
			}
		}
	}
	
	override fun toString(): String
	{
		return StringBuilder().apply {
			dataFiles.forEach { append("File Data - $it") }
			append("build.gradle:\n$gradleFile")
		}.toString()
	}
	
	inner class GradleFile(bytes: ByteArray, outPath: String = "")
	{
		var dataFile: DataFile
			private set
		
		init
		{
			val entryPath = "$outPath${File.separator}${"build.gradle"}"
			dataFile = DataFile(entryPath, bytes)
		}
		
		// TODO refactor
		fun removeExtensions(listToClean: Collection<Constants.Extensions>)
		{
			val gradleContent = dataFile.content
			val builder = StringBuilder()
			for (line in gradleContent.lines())
			{
				val isContainingLineToRm = listToClean.any { line.contains(it.dependency) }
				if (!isContainingLineToRm) builder.append("$line\n")
			}
			dataFile.content = builder.toString()
		}
		
		fun removeSubProjects(subProjectsToClean: Collection<Constants.SubProject>)
		{
			val gradleContent = dataFile.content
			
			val lines = StringBuilder()
			var counter = 0
			var read = true
			
			var waitForBracket = false
			var wrapped = false
			
			for (line in gradleContent.lines())
			{
				if (line.containsOneOfModule((subProjectsToClean))) waitForBracket = true
				
				if (waitForBracket)
				{
					counter += line.count { it == '{' }
					
					if (counter > 0)
					{
						wrapped = true
						read = false
					}
					
					if (wrapped && counter <= 0)
					{
						read = true
						wrapped = false
						waitForBracket = false
					}
					
					counter -= line.count { it == '}' }
				}
				
				if (read) lines.appendln(line)
				if (!read) lines.appendln("//$line")
			}
			
			dataFile.content = lines.toString()
		}
		
		private fun CharSequence.containsOneOfModule(collection: Collection<Constants.SubProject>): Boolean
		{
			return collection.any { this.contains("project(\":${it.module}\")") }
		}
		
		override fun toString(): String
		{
			return dataFile.content
		}
	}
}

