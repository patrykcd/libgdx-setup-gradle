package com.github.patrykcd.libgdxSetupPlugin.gdx

import com.intellij.history.core.Paths

data class DataFile(var path: String, var bytes: ByteArray)
{
	var content: String
		get() = String(bytes)
		set(value)
		{
			if (isBinaryFile()) throw Exception("binary file should not be modified")
			bytes = value.toByteArray()
		}
	
	val name: String
		get() = Paths.getNameOf(path)
	
	// check that file name has [.jpg, .jar, ...] extension
	fun isBinaryFile(): Boolean
	{
		arrayOf(".jpg", ".png", ".so", ".jar").forEach { fileExt ->
			if (name.endsWith(fileExt, true)) return true
		}
		return false
	}
	
	override fun toString(): String
	{
		return "out path: $path, bytes: $bytes, isBinary: ${isBinaryFile()}\n"
	}
	
//	// generated
//	override fun equals(other: Any?): Boolean
//	{
//		if (this === other) return true
//		if (javaClass != other?.javaClass) return false
//
//		other as DataFile
//
//		if (path != other.path) return false
//		if (!Arrays.equals(bytes, other.bytes)) return false
//
//		return true
//	}
//
//	// generated
//	override fun hashCode(): Int
//	{
//		var result = path.hashCode()
//		result = 31 * result + Arrays.hashCode(bytes)
//		return result
//	}
}