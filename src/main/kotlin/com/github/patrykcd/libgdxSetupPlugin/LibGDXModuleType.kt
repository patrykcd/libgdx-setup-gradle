package com.github.patrykcd.libgdxSetupPlugin

import com.intellij.openapi.module.ModuleType
import com.intellij.openapi.module.ModuleTypeManager
import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

class LibGDXModuleType : ModuleType<LibGDXModuleBuilder>(ID)
{
	companion object
	{
		private const val ID = "LIB_GDX_MODULE_TYPE"
		val instance: LibGDXModuleType
			get() = ModuleTypeManager.getInstance().findByID(ID) as LibGDXModuleType
	}
	
	override fun createModuleBuilder(): LibGDXModuleBuilder
	{
		return LibGDXModuleBuilder()
	}
	
	override fun getName(): String = "LibGDX Setup Gradle"
	
	override fun getDescription(): String = "This is unofficial LibGDX Setup Gradle plugin for fast project creation."
	
	override fun getNodeIcon(isOpened: Boolean): Icon = IconLoader.getIcon("/icon/libGDX_Setup_Gradle_icon.png")//AllIcons.General.Information
	
//	override fun createWizardSteps(
//			wizardContext: WizardContext,
//			moduleBuilder: LibGDXModuleBuilder,
//			modulesProvider: ModulesProvider)
//			: Array<ModuleWizardStep>
//	{
//		return super.createWizardSteps(wizardContext, moduleBuilder, modulesProvider)
//	}
}