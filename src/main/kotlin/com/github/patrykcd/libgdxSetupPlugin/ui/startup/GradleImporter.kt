package com.github.patrykcd.libgdxSetupPlugin.ui.startup

import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import com.intellij.ide.actions.ImportModuleAction
import com.intellij.ide.util.newProjectWizard.AddModuleWizard
import com.intellij.openapi.components.ServiceManager
import com.intellij.openapi.externalSystem.service.project.ProjectDataManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.StartupActivity
import org.jetbrains.plugins.gradle.service.project.wizard.GradleProjectImportBuilder
import org.jetbrains.plugins.gradle.service.project.wizard.GradleProjectImportProvider

class GradleImporter : StartupActivity
{
	override fun runActivity(project: Project)
	{
		val isNewLibGDXProject = project.getUserData(Constants.NEW_LIBGDX_PROJECT_KEY) ?: false // check that is it a newly created LibGDX Project
		println("is new libGDX project: $isNewLibGDXProject")
		
		if (isNewLibGDXProject)
		{
			val projectDataManager = ServiceManager.getService(ProjectDataManager::class.java)
			val gradleProjectImportBuilder = GradleProjectImportBuilder(projectDataManager)
			val gradleProjectImportProvider = GradleProjectImportProvider(gradleProjectImportBuilder)
			val wizard = AddModuleWizard(project, project.basePath, gradleProjectImportProvider)
			wizard.show()
			ImportModuleAction.createFromWizard(project, wizard)
		}
	}
}