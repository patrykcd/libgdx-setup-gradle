package com.github.patrykcd.libgdxSetupPlugin.ui

import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import com.intellij.ui.TitledSeparator
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBLabel
import com.intellij.ui.components.JBTextField
import java.awt.*
import javax.swing.BoxLayout
import javax.swing.JPanel

class LibGDXWizardStepPanel : JPanel()
{
	private val allComponents: Array<Component>
		get() = findAllComponents(components)
	
	var nameTextComponent: JBTextField
	var packageTextComponent: JBTextField
	var gameClassTextComponent: JBTextField
	
	val checkboxes: List<JBCheckBox>
		get() = allComponents.filterIsInstance<JBCheckBox>()
	
	init
	{
		// set layout for main panel
		layout = GridBagLayout()
		
		
		// set up container for main project
		val mainProjectContainer = Container().apply {
			layout = BoxLayout(this, BoxLayout.PAGE_AXIS)
			preferredSize = Dimension(this.width, 90)
			add(newLabelTextFieldContainer(Constants.FIELD_NAME))
			add(newLabelTextFieldContainer(Constants.FIELD_PACKAGE))
			add(newLabelTextFieldContainer(Constants.FIELD_GAME_CLASS))
		}
		
		// set up titled separator for main project
		val mainProjectSeparator = TitledSeparator("Main Project").apply {
			add(mainProjectContainer)
		}
		
		
		// set up checkboxes for sub projects
		val subProjects = Constants.SubProject.values().map { it.name }
		val subProjectsContainer = newCheckBoxesContainer(subProjects.toTypedArray())
		
		// set up titled separator for sub projects
		val subProjectsSeparator = TitledSeparator("Sub Projects").apply {
			add(subProjectsContainer)
		}
		
		
		// set up checkboxes for extensions
		val extensions = Constants.Extensions.values().map { it.name }
		val extensionsContainer = newCheckBoxesContainer(extensions.toTypedArray())
		
		// set up titled separator for extensions
		val extensionsSeparator = TitledSeparator("Extensions").also {
			add(extensionsContainer)
		}
		
		
		// set up constraints properties for GridBagLayout
		val gridBagProperties = GridBagConstraints().apply {
			anchor = GridBagConstraints.NORTHWEST
			fill = GridBagConstraints.HORIZONTAL
			weightx = 1.0
			weighty = 0.0
			gridx = 0
		}
		
		val gridBagPropertiesNotFill = gridBagProperties.clone() as GridBagConstraints
		gridBagPropertiesNotFill.fill = GridBagConstraints.NONE
		
		
		// add containers to setup panel
		// main project
		add(mainProjectSeparator, gridBagProperties)
		add(mainProjectContainer, gridBagProperties)
		
		// sub projects
		add(subProjectsSeparator, gridBagProperties)
		add(subProjectsContainer, gridBagPropertiesNotFill)
		
		gridBagPropertiesNotFill.weighty = 1.0
		
		// extensions
		add(extensionsSeparator, gridBagProperties)
		add(extensionsContainer, gridBagPropertiesNotFill)
		
		
		// assign component members
		nameTextComponent = findTextFieldComponent(Constants.FIELD_NAME)
		packageTextComponent = findTextFieldComponent(Constants.FIELD_PACKAGE)
		gameClassTextComponent = findTextFieldComponent(Constants.FIELD_GAME_CLASS)
	}
	
	// new Container with label and text field
	private fun newLabelTextFieldContainer(name: String): Container
	{
		return Container().apply {
			layout = BoxLayout(this, BoxLayout.LINE_AXIS)
			layout.preferredLayoutSize(this)
			
			JBLabel("$name: ").also {
				it.preferredSize = Dimension(90, it.size.height)
				it.name = null
				add(it)
			}
			
			JBTextField().also {
				it.name = name
				add(it)
			}
		}
	}
	
	private fun newCheckBoxesContainer(names: Array<String>): Container
	{
		return Container().apply {
			layout = GridLayout(0, 4, 20, 5)
			
			for (name in names)
			{
				JBCheckBox(name).also {
					it.name = name
					add(it)
				}
			}
		}
	}
	
	// get all components from all children
	private fun findAllComponents(components: Array<Component>): Array<Component>
	{
		val allComponents = ArrayList<Component>()
		for (component in components)
		{
			if (component.name != null) allComponents.add(component)
			// Go into component which doesn't have name (is null) and probably is a Container
			// WARN: Doesn't work if component have set name
			else // component.name is null
			{
				val container = component as Container
				val componentsFromContainer = findAllComponents(container.components)
				allComponents.addAll(componentsFromContainer)
			}
		}
		return allComponents.toTypedArray()
	}
	
	private fun findTextFieldComponent(field: String): JBTextField
	{
		return allComponents
				.filterIsInstance<JBTextField>()
				.first { it.name == field }
	}

/*
	// return "-iOS" if ios sub project (RoboVM, MOE)
	private fun iosSuffix(name: String): String
	{
		return if (name == Constants.SubProject.RoboVM.name || name == Constants.SubProject.MOE.name) name + "-iOS"
		else name
	}
*/
}