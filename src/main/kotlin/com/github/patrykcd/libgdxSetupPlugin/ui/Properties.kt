package com.github.patrykcd.libgdxSetupPlugin.ui

import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import com.intellij.ide.util.PropertiesComponent

object Properties
{
//	private const val NAME_TEXT_KEY: String = "${Constants.PLUGIN_ID}_CHECKBOXES_STATE_KEY"
	private const val PACKAGE_TEXT_KEY: String = "${Constants.PLUGIN_ID}_PACKAGE_TEXT_KEY"
//	private const val GAME_CLASS_TEXT_KEY: String = "${Constants.PLUGIN_ID}_GAME_CLASS_TEXT_KEY"
	
	private const val CHECKBOXES_STATE_KEY: String = "${Constants.PLUGIN_ID}_CHECKBOXES_STATE_KEY"
	
	private val properties = PropertiesComponent.getInstance()

//	var nameText: String
//		get() = properties.getValue(NAME_TEXT_KEY, Constants.DEFAULT_NAME_TEXT)
//		set(value) = properties.setValue(NAME_TEXT_KEY, value)
	
	var packageText: String
		get() = properties.getValue(PACKAGE_TEXT_KEY, Constants.DEFAULT_PACKAGE_TEXT)
		set(value)
		{
			val text = if (value.isBlank()) Constants.DEFAULT_PACKAGE_TEXT else value
			properties.setValue(PACKAGE_TEXT_KEY, text)
		}

//	var gameClassText: String
//		get() = properties.getValue(GAME_CLASS_TEXT_KEY, Constants.DEFAULT_GAME_CLASS_TEXT)
//		set(value) = properties.setValue(GAME_CLASS_TEXT_KEY, value)
	
	var checkboxesState: Array<Boolean>
		get()
		{
			val values = properties.getValues(CHECKBOXES_STATE_KEY)
			val checkboxesState = values?.map { it.toBoolean() }
			val size = Constants.SubProject.values().size + Constants.Extensions.values().size
			return checkboxesState?.toTypedArray() ?: Array(size, { false })
		}
		set(value)
		{
			val strArr = value.map { it.toString() }.toTypedArray()
			properties.setValues(CHECKBOXES_STATE_KEY, strArr)
		}
}
