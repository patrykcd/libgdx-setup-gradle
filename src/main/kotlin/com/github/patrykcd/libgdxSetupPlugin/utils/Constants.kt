package com.github.patrykcd.libgdxSetupPlugin.utils

import com.intellij.openapi.util.Key

object Constants
{
	const val PLUGIN_ID = "LIBGDX_SETUP_GRADLE_PLUGIN_ID"
	val NEW_LIBGDX_PROJECT_KEY = Key.create<Boolean>("LIBGDX_PROJECT_KEY")
	
	const val FIELD_NAME = "Game name"
	const val FIELD_PACKAGE = "Package"
	const val FIELD_GAME_CLASS = "Game class"
	
	const val DEFAULT_NAME_TEXT = "my-gdx-game"
	const val DEFAULT_PACKAGE_TEXT = "com.mygdx.game"
	const val DEFAULT_GAME_CLASS_TEXT = "MyGdxGame"
	
	const val JAR_RESOURCES_PATH = "templates"

//	const val TEMPLATE_MAIN_PROJECT = "main-project.zip"
//	const val TEMPLATE_DESKTOP = "desktop.zip"
//	const val TEMPLATE_ANDROID = "android.zip"
//	const val TEMPLATE_ROBOVM = "ios.zip"
//	const val TEMPLATE_MOE = "ios-moe.zip"
//	const val TEMPLATE_HTML = "html.zip"
	
	enum class SubProject(var module: String) // "var module" is root folder name of module
	{
		Desktop("desktop"),
		Android("android"),
		RoboVM("ios"),
		MOE("ios-moe"),
		HTML("html");
		
		companion object
		{
			fun isSubProject(name: String): Boolean
			{
				SubProject
						.values()
						.forEach { if (it.name == name) return true }
				return false
			}
		}
	}
	
	private const val VENDOR = "com.badlogicgames.gdx"
	
	enum class Extensions(var dependency: String)
	{
		Box2d("$VENDOR:gdx-box2d"),
		Bullet("$VENDOR:gdx-bullet");
		
		companion object
		{
			fun isExtension(name: String): Boolean
			{
				Extensions
						.values()
						.forEach { if (it.name == name) return true }
				return false
			}
		}
	}
}


/*
"%APP_NAME%"            , appName);
"%APP_NAME_ESCAPED%"    , appName.replace("'", "\\'"));
"%LANG%"                , language.name);
"%PACKAGE%"             , packageName);
"%PACKAGE_DIR%"         , packageDir);
"%MAIN_CLASS%"          , mainClass);
"%ANDROID_SDK%"         , sdkPath);
"%ASSET_PATH%"          , assetPath);
"%BUILD_TOOLS_VERSION%" , DependencyBank.buildToolsVersion);
"%API_LEVEL%"           , DependencyBank.androidAPILevel);
"%GWT_VERSION%"         , DependencyBank.gwtVersion);
"%GWT_INHERITS%"        , parseGwtInherits(builder));
*/