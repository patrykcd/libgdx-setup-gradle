package com.github.patrykcd.libgdxSetupPlugin.utils

import java.io.File
import java.io.InputStream
import java.util.zip.ZipInputStream

object JarResources
{
	// get zip file as ZipInputStream from resources in jar file
	fun getZipInputStream(zipFileName: String): ZipInputStream
	{
//		val inPath = Constants.JAR_RESOURCES_PATH
//		println("get zip file from $inPath/$zipFileName")
//		val classLoader = this::class.java.classLoader
//		val stream = classLoader.getResourceAsStream("$inPath${File.separator}$zipFileName")
		val inputStream = getInputStream(zipFileName)
		return ZipInputStream(inputStream)
	}
	
	fun getFileBytes(fileName: String): ByteArray
	{
		//val file = File(fileName)
		val inputStream = getInputStream(fileName)
	//	val bytes = inputStream.readBytes()
		//return inputStream
		//return String(bytes)
		
		
		
		return inputStream.readBytes()
	}
	
	private fun getInputStream(fileName: String): InputStream
	{
		val inPath = Constants.JAR_RESOURCES_PATH
		println("JarResources: get zip file from $inPath/$fileName")
		val classLoader = this::class.java.classLoader
		return classLoader.getResourceAsStream("$inPath${File.separator}$fileName")
	}
}