package com.github.patrykcd.libgdxSetupPlugin

import com.github.patrykcd.libgdxSetupPlugin.ui.LibGDXWizardStepPanel
import com.github.patrykcd.libgdxSetupPlugin.ui.Properties
import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import com.intellij.ide.util.projectWizard.ModuleWizardStep
import com.intellij.openapi.project.ProjectManager
import com.intellij.psi.PsiNameHelper
import com.intellij.psi.impl.file.PsiDirectoryFactory
import javax.swing.JComponent
import javax.swing.JOptionPane

class LibGDXModuleWizardStep : ModuleWizardStep()
{
	private val libGDXWizardStepPanel = LibGDXWizardStepPanel()
	
	override fun validate(): Boolean
	{
		val project = ProjectManager.getInstance().defaultProject
		val gameClassText = libGDXWizardStepPanel.gameClassTextComponent.text
		val packageText = libGDXWizardStepPanel.packageTextComponent.text
		
		return when
		{
			!PsiDirectoryFactory.getInstance(project).isValidPackageName(packageText) ->
			{
				JOptionPane.showMessageDialog(libGDXWizardStepPanel, "\"$packageText\": not a valid package name")
				false
			}
			!PsiNameHelper.getInstance(project).isQualifiedName(gameClassText) ->
			{
				JOptionPane.showMessageDialog(libGDXWizardStepPanel, "\"$gameClassText\": not a valid class name")
				false
			}
			else -> true
		}
	}
	
	private var LibGDXWizardStepPanel.checkboxesState: Array<Boolean>
		get()
		{
			val proprieties = ArrayList<Boolean>()
			checkboxes
					.forEach { component ->
						proprieties.add(component.isSelected)
					}
			
			return proprieties.toTypedArray()
		}
		set(value)
		{
			checkboxes
					.forEachIndexed { index, checkBox ->
						checkBox.isSelected = value[index]
					}
		}
	
	init
	{
		libGDXWizardStepPanel.apply {
			nameTextComponent.text = Constants.DEFAULT_NAME_TEXT
			gameClassTextComponent.text = Constants.DEFAULT_GAME_CLASS_TEXT
			
			packageTextComponent.text = Properties.packageText
			checkboxesState = Properties.checkboxesState
		}
	}
	
	override fun getComponent(): JComponent
	{
		return libGDXWizardStepPanel
	}
	
	override fun updateDataModel()
	{
		Properties.apply {
			packageText = libGDXWizardStepPanel.packageTextComponent.text
			checkboxesState = libGDXWizardStepPanel.checkboxesState
		}
	}
}