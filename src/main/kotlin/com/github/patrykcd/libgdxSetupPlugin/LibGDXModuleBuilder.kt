package com.github.patrykcd.libgdxSetupPlugin

import com.github.patrykcd.libgdxSetupPlugin.gdx.LibGDXProject
import com.github.patrykcd.libgdxSetupPlugin.ui.LibGDXWizardStepPanel
import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import com.github.patrykcd.libgdxSetupPlugin.utils.JarResources
import com.intellij.ide.util.projectWizard.ModuleBuilder
import com.intellij.ide.util.projectWizard.ModuleWizardStep
import com.intellij.ide.util.projectWizard.WizardContext
import com.intellij.openapi.Disposable
import com.intellij.openapi.module.ModuleType
import com.intellij.openapi.roots.ModifiableRootModel


class LibGDXModuleBuilder : ModuleBuilder()
{
	private var libGDXModuleWizardStep = LibGDXModuleWizardStep()
	
	override fun setupRootModel(modifiableRootModel: ModifiableRootModel)
	{
		val project = modifiableRootModel.project
		val baseDir = project.baseDir
		
		// get from jar resources main project template
		val gdxTemplateStream = JarResources.getZipInputStream("resources.zip")//(Constants.TEMPLATE_MAIN_PROJECT)
		val gdxGradleBytes = JarResources.getFileBytes("build.gradle")
		val gdxTemplate = LibGDXProject(gdxTemplateStream, gdxGradleBytes, baseDir.path)
		
		val wizardStep = libGDXModuleWizardStep.component as LibGDXWizardStepPanel
		val checkboxes = wizardStep.checkboxes
		
		// remove unselected (in wizard panel) sub project templates
		checkboxes
				.filterNot { it.isSelected }
				.filter { Constants.SubProject.isSubProject(it.name) }
				.forEach { checkbox ->
					val subProject = Constants.SubProject.valueOf(checkbox.name)
					gdxTemplate.removeSubProject(subProject)
				}
		
		checkboxes
				.filterNot { it.isSelected }
				.filter { Constants.Extensions.isExtension(it.name) }
				.forEach { checkbox->
					val extension = Constants.Extensions.valueOf(checkbox.name)
					gdxTemplate.removeExtensions(extension)
				}
		
	//	println(gdxTemplate)
		//	gdxTemplate.dataFiles.forEach { println(it.path) }
		gdxTemplate.create()
		baseDir?.let(modifiableRootModel::addContentEntry) // add files to Project view
	
	}
	
	override fun getModuleType(): ModuleType<*>
	{
		return LibGDXModuleType.instance
	}
	
	override fun getCustomOptionsStep(context: WizardContext, parentDisposable: Disposable): ModuleWizardStep
	{
		return libGDXModuleWizardStep
	}
}


/*

	override fun setupRootModel(modifiableRootModel: ModifiableRootModel)
	{
		val project = modifiableRootModel.project
		val baseDir = project.baseDir
		
		val templates = mapOf(
				Constants.SubProject.Desktop to Constants.TEMPLATE_DESKTOP,
				Constants.SubProject.Android to Constants.TEMPLATE_ANDROID,
				Constants.SubProject.RoboVM to Constants.TEMPLATE_ROBOVM,
				Constants.SubProject.MOE to Constants.TEMPLATE_MOE,
				Constants.SubProject.HTML to Constants.TEMPLATE_HTML)
		
		val projectsQueue = ArrayList<LibGDXProject>(templates.size)
		
		// get from jar resources main project template
		val mainProjectStream = JarResources.getZipInputStream(Constants.TEMPLATE_MAIN_PROJECT)
		val mainProject = LibGDXProject(mainProjectStream, baseDir.path)
		projectsQueue.add(mainProject)
		
		val wizardStep = libGDXModuleWizardStep.component as LibGDXWizardStepPanel
		val checkboxes = wizardStep.checkboxes
		
		// get from jar resources selected (in wizard panel) sub project templates
		checkboxes
				.filter { it.isSelected }
				.forEach { component ->
					templates
							.filter { it.key.name == component.name }
							.forEach { template ->
								val zipStream = JarResources.getZipInputStream(template.value)
								val subProject = LibGDXProject(zipStream, baseDir.path)
								projectsQueue.add(subProject)
								
								println("created " + component.name)
							}
				}
		
		// list unused extensions dependencies in libgdx projects (from checkboxes)
		val unusedExtensions = checkboxes
				.filterNot { it.isSelected }
				.filter { checkBox ->
					Constants.Extensions.values().any { extension ->
						checkBox.name == extension.name
					}
				}
				.map { Constants.Extensions.valueOf(it.name) }
		
		println("unused extensions: $unusedExtensions")
		
		// Remove unused extensions dependencies in libgdx projects
		projectsQueue.forEach {
			it.dataFiles
					.filterNot { it.isBinaryFile() }
					.forEach {
						it.content = DependencyRemover().clean(it.content, unusedExtensions)
					}
		}
		
		// list unused sub projects dependencies in libgdx projects (from checkboxes)
		val unusedSubProjects = checkboxes
				.filterNot { it.isSelected }
				.filter { checkBox ->
					Constants.SubProject.values().any { extension ->
						checkBox.name == extension.name
					}
				}
				.map { Constants.SubProject.valueOf(it.name) }
		
		println("unused sub projects: $unusedSubProjects")
		
		// Remove unused sub projects in libgdx projects
		projectsQueue.forEach {
			it.dataFiles
					.filterNot { it.isBinaryFile() }
					.forEach {
						it.content = SubProjectRemover().clean(it.content, unusedSubProjects)
					}
		}
		
		// rename name, package and game class in all files
		projectsQueue.forEach {
			it.dataFiles
					.filterNot { it.isBinaryFile() }
					.forEach {
						it.content = it.content.replace(Constants.DEFAULT_NAME_TEXT, wizardStep.nameTextComponent.text)
						it.content = it.content.replace(Constants.DEFAULT_PACKAGE_TEXT, wizardStep.packageTextComponent.text)
						it.content = it.content.replace(Constants.DEFAULT_GAME_CLASS_TEXT, wizardStep.gameClassTextComponent.text)
					}
		}
		
		// make folder structure relevant to package
		// and rename class files
		val defaultPackageFolderStructure = Constants.DEFAULT_PACKAGE_TEXT.replace('.', '/')
		val newPackageFolderStructure = wizardStep.packageTextComponent.text.replace('.', '/')
		projectsQueue.forEach {
			it.dataFiles.forEach {
				it.path = it.path.replace(defaultPackageFolderStructure, newPackageFolderStructure)
				it.path = it.path.replace(Constants.DEFAULT_GAME_CLASS_TEXT, wizardStep.gameClassTextComponent.text)
			}
		}
		
		projectsQueue.forEach { it.create() } // create Project Files
		
		// create symlink in android folder linked to core assets
		if (!unusedSubProjects.contains(Constants.SubProject.Android))
		{
			val symlink = Paths.get("${baseDir.path}/android/assets")
			val original = Paths.get("../core/assets")
			Files.createSymbolicLink(symlink, original);
		}
		
		baseDir?.let(modifiableRootModel::addContentEntry) // add files to Project view
//		ExternalSystemUtil.refreshProject(project, GradleConstants.SYSTEM_ID, baseDir.path, false, ProgressExecutionMode.IN_BACKGROUND_ASYNC) // import gradle on startup
		PropertiesComponent.getInstance(project).setValue("show.inlinked.gradle.project.popup", false, true) // hide gradle import notification
		project.putUserData(Constants.NEW_LIBGDX_PROJECT_KEY, true) // set new LibGDX Project Flag for GradleImporter
	}

*/