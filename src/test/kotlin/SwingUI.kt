import com.github.patrykcd.libgdxSetupPlugin.ui.LibGDXWizardStepPanel
import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import javax.swing.JFrame


fun main(args: Array<String>)
{
	val wizardStep = LibGDXWizardStepPanel()
	
	val frame = JFrame("LibGDX Setup Gradle Plugin UI")
	frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
	frame.contentPane = wizardStep
	frame.setSize(600, 500)
	frame.isVisible = true
	
	val box2d = wizardStep.checkboxes
			.first { it.name == Constants.Extensions.Box2d.name }
	
	box2d.isSelected = true
	box2d.isEnabled = false
}
