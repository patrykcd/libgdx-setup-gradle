import com.github.patrykcd.libgdxSetupPlugin.gdx.LibGDXProject
import com.github.patrykcd.libgdxSetupPlugin.utils.Constants
import java.io.File
import java.io.FileInputStream
import java.util.zip.ZipInputStream


//TODO parse http://central.maven.org/maven2/com/badlogicgames/gdx/gdx/1.9.8/

fun main(args: Array<String>)
{
	val outPath = File("src/test/assets").path
	
	val file = FileInputStream("/Users/patrykcd/Projects/Java/libGDX Setup Gradle/src/test/resources/templates/resources.zip")
	val zipStream = ZipInputStream(file)
	// JarResources.getZipInputStream("resources.zip")
	
	val gradleFile = FileInputStream("/Users/patrykcd/Projects/Java/libGDX Setup Gradle/src/test/resources/templates/build.gradle")
	
	val project = LibGDXProject(zipStream, gradleFile.readBytes(), outPath)
	project.removeSubProject(Constants.SubProject.HTML)
	project.removeSubProject(Constants.SubProject.Android)
	project.removeSubProject(Constants.SubProject.RoboVM)
	
	//project.dataFiles.forEach { println(it.path) }
	
	
	//println(String(gradleFile.readBytes()))
//	val gradle = LibGDXGradle(gradleFile.readBytes())
//	println(gradle)
	
	
	//project.create()
//	println("content: ")
	//project.names.forEach(::println)
//	desktop.dataFiles.forEach {
//		it.path = it.path.replace("com/mygdx/game", "my/path")
//		it.path.also { println("path $it") }
//	}
//	val outPath = File("src/test/assets").path
//	val zipStream = JarResources.getZipInputStream("android.zip")
//	val desktop = LibGDXProject(zipStream, outPath)
//	println("content: ")
	//println()
//	desktop.dataFiles.forEach {
//		it.path = it.path.replace("com/mygdx/game", "my/path")
//		it.path.also { println("path $it") }
//	}


//	val fullPath = "example/path/file.txt"

//	print(Paths.get(fullPath).parent.toString())
//	desktop.create()


//	val symlink = Paths.get("$outPath/android/assets")
//	val original = Paths.get("../core/assets")
//	Files.createSymbolicLink(symlink,original).also { println("the path to the symbolic link: " + it) }
	//desktop.


//	val templates = mapOf(
//			Constants.SubProject.Desktop to Constants.TEMPLATE_DESKTOP,
//			Constants.SubProject.Android to Constants.TEMPLATE_ANDROID,
//			Constants.SubProject.RoboVM to Constants.TEMPLATE_ROBOVM,
//			Constants.SubProject.MOE to Constants.TEMPLATE_MOE,
//			Constants.SubProject.HTML to Constants.TEMPLATE_HTML)
//
//	for (template in templates)
//	{
//		val stream = JarResources.getZipInputStream(template.value)
//		val project = LibGDXProject(stream, outPath)
//		//println(project)
//		println("\n${template.key}:")
//		project.names.forEach(::println)
//	}
}